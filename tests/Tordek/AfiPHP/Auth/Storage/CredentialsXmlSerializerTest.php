<?php
/**
 * AfiPHP
 *
 * PHP Version 5.4
 *
 * @copyright 2015 Guillermo O. "Tordek" Freschi
 * @license MIT
 */

namespace Tordek\AfiPHP\Auth\Storage;

use Tordek\AfiPHP\Auth\Credenciales;

class CredencialesXmlSerializerTest extends \PHPUnit_Framework_TestCase
{
    public function testCanReadBackWhatItStored()
    {
        // TODO: use an actual generator.

        $credenciales = new Credenciales(
            rand(),
            rand(),
            new \DateTimeImmutable(),
            new \DateTimeImmutable("now + 1 day")
        );

        $serializer = new CredencialesXmlSerializer();

        $this->assertEquals(
            $credenciales,
            $serializer->parse(
                $serializer->serialize(
                    $credenciales
                )
            )
        );
    }
}
